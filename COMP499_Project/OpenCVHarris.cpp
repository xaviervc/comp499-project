//#include <opencv2/opencv.hpp>
//#include <opencv2/features2d.hpp>
//#include <iostream>
//
//
//using namespace cv;
//using namespace std;
//
//int main() {
//
//	Mat src1 = imread("project_images/Rainier1.png", IMREAD_GRAYSCALE);
//	Mat src2 = imread("project_images/Rainier2.png", IMREAD_GRAYSCALE);
//	
//	Mat src1_gray, src2_gray;
//
//	if (src1.empty() || src2.empty()) {
//		return -1;
//	}
//
//	//taken from opencv.org
//	int blockSize = 2;
//	int apertureSize = 3;
//	double k = 0.04;
//	int thresh = 200;
//
//	Mat dst1 = Mat::zeros(src1.size(), CV_32FC1);
//	Mat dst2 = Mat::zeros(src2.size(), CV_32FC1);
//
//	cornerHarris(src1, dst1, blockSize, apertureSize, k);
//	cornerHarris(src2, dst2, blockSize, apertureSize, k);
//
//	Mat dst1_norm, dst2_norm, dst1_norm_scaled, dst2_norm_scaled;
//	normalize(dst1, dst1_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
//	normalize(dst2, dst2_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());
//
//	convertScaleAbs(dst1_norm, dst1_norm_scaled);
//	convertScaleAbs(dst2_norm, dst2_norm_scaled);
//
//	for (int i = 0; i < dst1_norm.rows; i++) {
//		for (int j = 0; j < dst1_norm.cols; j++) {
//			if ((int) dst1_norm.at<float>(i, j) > thresh) {
//				circle(dst1_norm_scaled, Point(j, i), 5, Scalar(0), 2, 8, 0);
//			}
//		}
//	}
//	for (int i = 0; i < dst2_norm.rows; i++) {
//		for (int j = 0; j < dst2_norm.cols; j++) {
//			if ((int)dst2_norm.at<float>(i, j) > thresh) {
//				circle(dst2_norm_scaled, Point(j, i), 5, Scalar(0), 2, 8, 0);
//			}
//		}
//	}
//
//	imshow("test", dst1_norm_scaled);
//	imshow("test1", dst2_norm_scaled);
//	
//
//	waitKey();
//
//	return 0;
//}