#include <opencv2/xfeatures2d.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>

using namespace cv;
using namespace cv::xfeatures2d;
using namespace std;

void RANSAC1(vector<Point2d>&, vector<Point2d>&, Mat&, Mat&, int, double, vector<Point2d>&, vector<Point2d>&);
void computeInlierCount(Mat&, vector<Point2d>&, vector<Point2d>&, double, int&);
void project(Point2d&, Mat&, Point2d&);

void stitch(Mat& src1, Mat& src2, Mat& hom, Mat& hom_inv, Mat& out_stitch);


int main() {

	Mat src1 = imread("project_images/Rainier1.png", IMREAD_GRAYSCALE);
	Mat src2 = imread("project_images/Rainier2.png", IMREAD_GRAYSCALE);


	Ptr<SIFT> detector = SIFT::create();

	vector<KeyPoint> keypoints1, keypoints2;

	Mat src1_descr, src2_descr;

	detector->detectAndCompute(src1,noArray(), keypoints1, src1_descr);
	detector->detectAndCompute(src2,noArray(), keypoints2, src2_descr);

	Ptr<BFMatcher> matcher = BFMatcher::create();

	vector<vector<DMatch>> matches;

	matcher->knnMatch(src1_descr, src2_descr, matches, 2);

	//Ratio Test Lowe
	vector<DMatch> good_matches;
	for (int i = 0; i < matches.size(); i++) {
		if (matches[i][0].distance < 0.75 * matches[i][1].distance) {
			good_matches.push_back(matches[i][0]);
		}
	}

	vector<Point2d> src1_points, src2_points;

	//Build Query Matrix and Train Matrix for RANSAC and findHomography
	//these points correspond to a good match between the Query and the Train image
	for (int i = 0; i < good_matches.size(); i++) {
		src1_points.push_back(keypoints1[good_matches[i].queryIdx].pt);
		src2_points.push_back(keypoints2[good_matches[i].trainIdx].pt);
	}

	vector<Point2d> out1_points, out2_points;
	
	Mat h, h_inv;

	RANSAC1(src1_points, src2_points, h, h_inv, 200, 0.05, out1_points, out2_points);


	//Mat h = findHomography(src1_points, src2_points, RANSAC);
	//Mat h = findHomography(src2_points, src1_points, RANSAC);

	//Mat dst = Mat::zeros(src2.size(), CV_32FC1);

	//warpPerspective(src2, dst, h, dst.size());

	Mat output;
	vector<DMatch> better_matches;
	vector<KeyPoint> out1_keypoints, out2_keypoints;

	for (int i = 0; i < out1_points.size(); i++) {
		out1_keypoints.push_back(KeyPoint(out1_points[i], 1));
		out2_keypoints.push_back(KeyPoint(out2_points[i], 1));
		better_matches.push_back(DMatch(i, i, i));
	}

	//drawMatches(src1, out1_keypoints, src2, out2_keypoints, better_matches, output);

	Mat out_stitched, src1_64, src2_64;

	src1.convertTo(src1_64, CV_64F, 1.0 / 255.0);
	src2.convertTo(src2_64, CV_64F, 1.0 / 255.0);


	stitch(src1_64, src2_64, h, h_inv, out_stitched);

	imshow("test", out_stitched);

	waitKey();

	//system("pause");

}

void RANSAC1(vector<Point2d>& src1_points, vector<Point2d>& src2_points, Mat& h, Mat& h_inv , int n_iter, double in_thresh, vector<Point2d>& out1_points, vector<Point2d>& out2_points) {


	int in_count = 0;
	int greatest_inlier_count = 0;

	int range = src1_points.size();
	int num;
	Mat possible_h;
	Mat best_h;

	for (; n_iter > 0; n_iter--) {

		vector<Point2d> src1_rand, src2_rand;

		for (int i = 0; i < 5; i++) {
			num = rand() % range;
			src1_rand.push_back(src1_points[num]);
			src2_rand.push_back(src2_points[num]);
		}

		possible_h = findHomography(src1_rand, src2_rand, 0);
		computeInlierCount(possible_h, src1_points, src2_points, in_thresh, in_count);

		if (in_count > greatest_inlier_count) {
			best_h = possible_h;
			greatest_inlier_count = in_count;
		}
	}

	//using best Homography refine the points

	vector<Point2d> src1_refined, src2_refined;

	for (int i = 0; i < src1_points.size(); i++) {

		Point2d test_point;
		project(src1_points[i], best_h, test_point);

		Point2d diff = src2_points[i] - test_point;


		//close to zero meaning that it is a good match
		if (sqrt(diff.x*diff.x + diff.y*diff.y) < in_thresh) {
			src1_refined.push_back(src1_points[i]);
			src2_refined.push_back(src2_points[i]);
		}

	}

	out1_points = src1_refined;
	out2_points = src2_refined;


	h = findHomography(src1_refined, src2_refined, 0);
	h_inv = findHomography(src2_refined, src1_refined, 0);
}

void computeInlierCount(Mat& h, vector<Point2d>& src1_points, vector<Point2d>& src2_points, double in_thresh, int& in_count) {

	for (int i = 0; i < src1_points.size(); i++) {

		Point2d test_point;
		project(src1_points[i], h, test_point);

		Point2d diff = src2_points[i] - test_point;

		//close to zero meaning that it is a good match
		if (sqrt(diff.x*diff.x + diff.y*diff.y) < in_thresh) {
			in_count++;
		}
		
	}

}

void project(Point2d& one, Mat& h, Point2d& two) {

	Mat src(3, 1, CV_64F);

	src.at<double>(0, 0) = one.x;
	src.at<double>(1, 0) = one.y;
	src.at<double>(2, 0) = 1.0;

	if (!h.empty()) {
		two.x = (h.at<double>(0, 0)*one.x + h.at<double>(0, 1)*one.y + h.at<double>(0, 2)) / (h.at<double>(2, 0)*one.x + h.at<double>(2, 1)*one.y + h.at<double>(2, 2));
		two.y = (h.at<double>(1, 0)*one.x + h.at<double>(1, 1)*one.y + h.at<double>(1, 2)) / (h.at<double>(2, 0)*one.x + h.at<double>(2, 1)*one.y + h.at<double>(2, 2));
	}
	else {
		two.x = 0.0;
		two.y = 0.0;
	}
}


void stitch(Mat& src1, Mat& src2, Mat& hom, Mat& hom_inv, Mat& out_stitch) {
	//compute size of out_stitch
	//top left point

	Point2d t_left(0.0, 0.0);
	Point2d t_right(src2.cols, 0.0);
	Point2d b_left(0.0, src2.rows);
	Point2d b_right(src2.cols, src2.rows);


	Point2d p_t_left, p_t_right, p_b_left, p_b_right;

	project(t_left, hom_inv, p_t_left);
	project(t_right, hom_inv, p_t_right);
	project(b_left, hom_inv, p_b_left);
	project(b_right, hom_inv, p_b_right);


	int width, height;

	if (p_t_right.x - p_t_left.x > p_b_right.x - p_b_left.x) {
		width = p_t_left.x - p_t_right.x;
	}
	else {
		width = p_b_left.x - p_b_right.x;
	}

	if (p_b_left.y - p_t_left.y > p_b_right.y - p_t_right.y) {
		height = p_b_left.y - p_t_left.y;
	}
	else {
		height = p_b_right.y - p_t_right.y;
	}

	//need to figure this out instead of hard coding
	width = 730;
	height = 456;


	out_stitch = Mat::zeros(height, width, CV_64F);

	for (int i = 0; i < src1.rows; i++) {
		for (int j = 0; j < src1.cols; j++) {
			out_stitch.at<double>(i, j) = src1.at<double>(i, j);
		}
	}

	for (int i = 0; i < src2.rows; i++) {
		for (int j = 0; j < src2.cols; j++) {

			Point2d in(j, i), out;

			project(in, hom_inv, out);

			int out_x = (int)out.x;
			int out_y = (int)out.y;


			if (out_x > 0 && out_x < width && out_y > 0 && out_y < height) {
				out_stitch.at<double>(out_y, out_x) = src2.at<double>(i, j);
			}
		}
	}
}