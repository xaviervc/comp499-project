//#include <opencv2/xfeatures2d.hpp>
//#include <opencv2/opencv.hpp>
//#include <iostream>
//#include <vector>
//
//using namespace cv;
//using namespace cv::xfeatures2d;
//using namespace std;
//
//int main() {
//
//	Mat src1 = imread("project_images/Rainier1.png", IMREAD_GRAYSCALE);
//	Mat src2 = imread("project_images/Rainier2.png", IMREAD_GRAYSCALE);
//
//	double minHessian = 400.0;
//
//	Ptr<SURF> detector = SURF::create(minHessian);
//
//	vector<KeyPoint> keypoints1, keypoints2;
//
//	Mat src1_descr, src2_descr;
//
//	detector->detectAndCompute(src1,noArray(), keypoints1, src1_descr);
//	detector->detectAndCompute(src2,noArray(), keypoints2, src2_descr);
//
//	Ptr<BFMatcher> matcher = BFMatcher::create();
//
//	vector<DMatch> matches;
//
//	matcher->match(src1_descr, src2_descr, matches);
//
//	Mat output;
//
//	drawMatches(src1, keypoints1, src2, keypoints2, matches, output);
//
//	imshow("surf", output);
//
//	waitKey();
//}